package com.shruti.trees.model;

import lombok.Data;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
public class BinaryNode {
	int key;
	BinaryNode left, right;
	
	public BinaryNode(int data) {
		this.key = data;
		left = right = null;
	}
}
