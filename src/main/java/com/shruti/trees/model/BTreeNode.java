package com.shruti.trees.model;
/**
 * 
 * @author shruti.mehrotra
 *
 */

public class BTreeNode {
	int[] keys;
	BTreeNode[] children;
	int numOfChildren;
	
	public BTreeNode(int[] data) {
		this.keys = data;
	}
}
