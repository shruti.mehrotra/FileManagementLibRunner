package com.shruti.trees;

import com.shruti.trees.model.BinaryNode;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class BinaryTreeTraversal {

	void preorder(BinaryNode root) {
		if (root == null)
			return;
		System.out.println(root.getKey());
		preorder(root.getLeft());
		preorder(root.getRight());
	}

	void postorder(BinaryNode root) {
		if (root == null)
			return;
		postorder(root.getLeft());
		postorder(root.getRight());
		System.out.println(root.getKey());
	}

	void inorder(BinaryNode root) {
		if (root == null) {
			return;
		}
		inorder(root.getLeft());
		System.out.println(root.getKey());
		inorder(root.getRight());
	}

	public static void main(String[] args) {
		BinaryNode node = new BinaryNode(1);
		BinaryNode left1 = new BinaryNode(2);
		BinaryNode left11 = new BinaryNode(5);
		BinaryNode leftright11 = new BinaryNode(7);
		left1.setLeft(left11);
		left1.setRight(leftright11);
		node.setLeft(left1);
		BinaryNode right1 = new BinaryNode(3);
		BinaryNode rightleft11 = new BinaryNode(9);
		BinaryNode rightright11 = new BinaryNode(10);
		right1.setLeft(rightleft11);
		right1.setRight(rightright11);
		node.setRight(right1);
		
		BinaryTreeTraversal treeTraversal = new BinaryTreeTraversal();
		System.out.println("==========================> Preorder (NLR)");
		treeTraversal.preorder(node);
		
		System.out.println("==========================> Postorder (LRN)");
		treeTraversal.postorder(node);
		
		System.out.println("==========================> Inorder (LNR)");
		treeTraversal.inorder(node);
	}
}
