package com.shruti;
/**
 * 
 * @author shruti.mehrotra
 *
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.freecharge.model.type.FileConfigType;
import com.freecharge.util.DateUtil;
import com.shruti.batch.handler.FileStatusManagementHandler;

@Component
public class MyTrigger {
	
	@Autowired
	FileStatusManagementHandler handler;
	
	@Scheduled(fixedDelay=1000)
	public void runJob() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(DateUtil.YYYYDDMM_HYPHENED_HHMMSS_COLONED);
		handler.processFile(FileConfigType.REFUND_ICICI.name(), sdf.parse("2015-01-20 00:00:00"), sdf.parse("2017-05-30 00:00:00"), "soshgds");
	}
}
