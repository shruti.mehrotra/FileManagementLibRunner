package com.shruti.search;
/**
 * 
 * @author shruti.mehrotra
 *
 */
public class BinarySearch {
	public int search(int[] arr, int key) {
		return binarySearch(arr, 0, arr.length-1, key);
	}
	
	private int binarySearch(int[] arr, int left, int right, int key) {
		if (left <= right) {
			int mid = (left + right)/2;
			if (arr[mid] == key) {
				return mid;
			}
			if (key < arr[mid]) {
				binarySearch(arr, left, mid-1, key);
			} else {
				binarySearch(arr, mid+1, right, key);
			}
		}
		return -1;
	}
}
