package com.shruti.multithreading;

public class ThreadDemo {
	public static void main(String[] args) {
		RunnableDemo r1 = new RunnableDemo("s1");
		r1.start();

		RunnableDemo r2 = new RunnableDemo("s2");
		r2.start();

		ThreadExtendedDemo t1 = new ThreadExtendedDemo("j1");
		t1.start();

		ThreadExtendedDemo t2 = new ThreadExtendedDemo("j2");
		t2.start();
	}

}
