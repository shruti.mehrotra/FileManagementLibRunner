package com.shruti.multithreading;

import java.awt.print.Printable;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class ThreadExtendedDemo extends Thread {
	private Thread t;
	   private String threadName;
	   
	   ThreadExtendedDemo( String name) {
	      threadName = name;
	      System.out.println("Creating " +  threadName );
	   }
	   
	   public void run() {
	      System.out.println("Running " +  threadName );
	      try {
	         for(int i = 4; i > 0; i--) {
	            System.out.println("Thread: " + threadName + ", " + i);
	            // Let the thread sleep for a while.
	            Thread.sleep(50);
	         }
	      }catch (InterruptedException e) {
	         System.out.println("Thread " +  threadName + " interrupted.");
	      }
	      System.out.println("Thread " +  threadName + " exiting.");
	   }
	   
	   public void start () {
	      System.out.println("Starting " +  threadName );
	      if (t == null) {
	         t = new Thread (this, threadName);
	         t.start ();
	      }
	   }
	   public static void main(String[] args) {
//		print(false);
	}
	  /*static void print (Integer i) {
		   System.out.println("Integer");
	   }*/
/*static void print (Object i) {
	System.out.println("Integer");
	   }*/
static void print (String i) {
	System.out.println("Integer");
}

static boolean print(int i) {
	return false;
	
}
}
