package com.shruti.multithreading;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class RunnableDemo implements Runnable {
	private Thread thread;
	private String threadName;

	public RunnableDemo(String threadName) {
		this.threadName = threadName;
		System.out.println("Creating thread : " + threadName);
		
	}
	
	public void run() {
		System.out.println("Running thread : " + threadName);

		try {
			for (int i = 0; i < 4; i++) {
				System.out.println("Thread: " + threadName + " , i: " + i);
				Thread.sleep(50);
			}
		} catch (InterruptedException e) {
			System.out.println("Thread : " + threadName + " interrupted");
		}
		System.out.println("Exiting thread : " + threadName);
	}

	public void start() {
		System.out.println("Starting thread: " + threadName);
		if (thread == null) {
			thread = new Thread(this, threadName);
			thread.start();
		}
	}

}
