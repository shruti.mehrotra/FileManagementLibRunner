package com.shruti.strings;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class CheckIfAnyAnagramIsPallindrom {
	public static boolean isPallindrome(String input) {
		int[] trackerArr = new int[26];
		
		for (int i=0;i<input.length();i++) {
			char ch = input.charAt(i);
			trackerArr[ch-'a']++;
		}
		
		int oddOccur = 0;
		for (int cnt:trackerArr) {
			if (oddOccur > 1) {
				return false;
			}
			if (cnt % 2 == 1) {
				oddOccur ++;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		System.out.println(isPallindrome("aaddaad"));
	}
}
