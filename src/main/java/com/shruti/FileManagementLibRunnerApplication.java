package com.shruti;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.batch.BatchAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.freecharge.config.AppConfig;
/**
 * Created by shruti.mehrotra on 16/5/16.
 */
@SpringBootApplication
@ImportResource("classpath:spring/application-context.xml")
@ComponentScan({ "com.freecharge", "com.shruti" })
@EnableBatchProcessing
@EnableScheduling
@Import(AppConfig.class)
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class, BatchAutoConfiguration.class})
public class FileManagementLibRunnerApplication {

   public static void main(String[] args) {
      SpringApplication.run(FileManagementLibRunnerApplication.class, args);
      System.out.println("Application started!");
      
   }

}
