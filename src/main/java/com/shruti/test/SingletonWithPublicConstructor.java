package com.shruti.test;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class SingletonWithPublicConstructor {
	private static SingletonWithPublicConstructor obj;
	private String str;

	public SingletonWithPublicConstructor() {
		if (obj != null) {
			throw new RuntimeException("Singleton already initialized!");
		}
	}
	
	public String getStr() {
	    return str;
	}

	public void setStr(String str) {
	    this.str = str;
	}

	public static SingletonWithPublicConstructor getInstance() {
		if (obj == null) {
			obj = new SingletonWithPublicConstructor();
		}
		return obj;
	}

	public static void main(String[] args) {
		SingletonWithPublicConstructor singleton = null;
		try {
			singleton = new SingletonWithPublicConstructor();
			singleton.setStr("Hello1");
			System.out.println(singleton.getStr());
		} catch (RuntimeException e) {

		}
		SingletonWithPublicConstructor single1 = SingletonWithPublicConstructor.getInstance();
//        single1.setHello("Hello");
        System.out.println(single1.getStr());

        SingletonWithPublicConstructor single2 = SingletonWithPublicConstructor.getInstance();
        System.out.println(single2.getStr());
        
        singleton = new SingletonWithPublicConstructor();
		System.out.println(singleton.getStr());
        System.out.println(singleton.getStr());
	}
}
