package com.shruti.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Supplier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.freecharge.dao.dto.KeyValWithDbOperatorDto;
import com.freecharge.dao.dto.KeyValWithDbOperatorList;
import com.freecharge.dao.dto.operators.DBOperator;
import com.freecharge.file.snapshot.IFileSnapshotter;
import com.freecharge.model.request.FileSnapshotRequest;
import com.freecharge.model.type.FileConfigType;

import lombok.Data;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@RestController
public class TestClass {
	@Autowired
	private IFileSnapshotter fileSnapshotter;
	
	@RequestMapping(name = "/test1", method = RequestMethod.GET)
	public void test() {
		FileConfigType fileConfigType = FileConfigType.REFUND_KOTAK;
		List<KeyValWithDbOperatorDto> keyValPairWithDbOperatorList = new ArrayList<>();
		keyValPairWithDbOperatorList.add(new KeyValWithDbOperatorDto("n_last_updated", "'2016-01-19 15:00:00'", DBOperator.GREATER_THAN, null));
		KeyValWithDbOperatorList filterForSnapshotRecords = new KeyValWithDbOperatorList(keyValPairWithDbOperatorList);
		fileSnapshotter.doSnapshot(new FileSnapshotRequest(fileConfigType, filterForSnapshotRecords, null));
	}
	
	public static void main(String[] args) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date date = sdf.parse("2017-06-08 15:34:00");
		System.out.println(date);
		System.out.println(date.getTime());
		
		Date date2 = sdf.parse("2017-05-31 15:15:00");
		System.out.println(date2);
		System.out.println(date2.getTime());
		
		
		List<Test> tList = new ArrayList<>();
		tList.add(new Test("s1"));
		tList.add(new Test("s2"));
		tList.add(new Test("s3"));
		tList.add(new Test("s4"));
		tList.add(new Test("s5"));
		tList.add(new Test("s6"));
		tList.stream()
				.filter(t -> !t.getS1().equals("s5"))
				.forEach(t -> System.out.println(t));
		
		List<Test> xList = new ArrayList<>();
		Supplier<Test> testSupp = () -> new Test("x1");
		xList.add(testSupp.get());
		System.out.println("list:");
		xList.stream().forEach(TestClass::print);
		
		Long l = 0L;
		System.out.println(l.equals(0L));
		System.out.println(l.equals(0));
	}
	
	static void fubar(long b) {
		System.out.println("1");
		}
		static void fubar(Long b) {
		System.out.println("2");
		}
	
	public static void print(Test x) {
		System.out.println("s1 = " + x.getS1());
	}
	
	@Data
	public static class Test {
		private String s1;
		
		public Test() {
			// TODO Auto-generated constructor stub
		}
		public Test(String s) {
			s1 = s;
		}
	}
}
