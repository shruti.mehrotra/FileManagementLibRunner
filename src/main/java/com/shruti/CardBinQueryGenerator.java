package com.shruti;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVReader;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class CardBinQueryGenerator {
	public static void main(String[] args) {
		String filePath = "/home/standby/Payments/rupayBinsToUpdate_final.csv";
		try {

			@SuppressWarnings("resource")
			CSVReader reader = new CSVReader(new FileReader(filePath));
			String[] nextLine;

			nextLine = reader.readNext();
			List<String[]> data = new ArrayList<>();
			while ((nextLine = reader.readNext()) != null) {
				String bin = nextLine[2];
				String issuerBankName = nextLine[4];
				try {
					String[] row = { "uuid()", "*", "RUPAY", "DEBIT", "Active",
							bin, "(select bank_code from issuer_code_mapping where bank_name = '" + issuerBankName +"')",
							"1", ""};
					data.add(row);
				} catch (Exception e) {
					System.out.println("Error occured Error: " + e);
				}
			}
			for (String[] row: data) {
				System.out.println(createInsertStatement(row));
			}
			
		} catch (Exception e) {
			System.out.println("Error occured while parsing Error: " + e);
		}
	}

	public static String createInsertStatement(String[] row) {
		StringBuilder insertStmt = new StringBuilder();
		insertStmt.append("INSERT INTO card_bin_range (bin_range_id, bin_tail, card_type, card_nature, status, bin_head, card_issuer, is_domestic, fk_card_issuer_id) VALUES (");
		int count = 0;
		for (Object val: row) {
			if (!(val instanceof Long) && !(val instanceof Double) && !(val instanceof Integer) && !val.toString().contains("select") && !val.toString().contains("()") && !val.toString().equals("1")) {
				insertStmt.append("'")
				.append(val).append("'");
			} else {
				insertStmt.append(val);
			}
			count ++;
			if (count < row.length) {
				insertStmt.append(",");
			}
		}
		insertStmt.append(");");
		return insertStmt.toString();
	}
}
