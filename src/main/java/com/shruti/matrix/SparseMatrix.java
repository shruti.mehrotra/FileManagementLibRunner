package com.shruti.matrix;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class SparseMatrix {
	// Sparse matrix can be representated either as an array or as linked list
	
	int[][] compress(int[][] matrix) {
		int size = 0;
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if (matrix[i][j] != 0) {
					size ++;
				}
			}
		}
		int compressedMatrix[][] = new int[3][size];
		int k = 0;
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if (matrix[i][j] != 0) {
					compressedMatrix[0][k] = i;
					compressedMatrix[1][k] = j;
					compressedMatrix[2][k] = matrix[i][j];
					k++;
				}
			}
		}
		return compressedMatrix;
	}
	
	public static void main(String[] args) {
		int matrix[][] = { 
				{ 0, 0, 3, 0, 4 }, 
				{ 0, 0, 5, 7, 0 }, 
				{ 0, 0, 0, 0, 0 }, 
				{ 0, 2, 6, 0, 0 }
		};
		SparseMatrix sparseMatrix = new SparseMatrix();
		int resultMatrix[][] = sparseMatrix.compress(matrix);
		
		for (int i = 0; i < resultMatrix.length; i++) {
			for (int j = 0; i < resultMatrix[i].length; j++) {
				System.out.print(resultMatrix[i][j]);
			}
			System.out.println("");
		}
		
	}
}
