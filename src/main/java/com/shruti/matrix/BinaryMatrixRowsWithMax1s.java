package com.shruti.matrix;

import com.shruti.search.BinarySearch;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class BinaryMatrixRowsWithMax1s {
	int getMaxOnesInMatrix(int[][] matrix) {
		int maxRowIndex = 0;
		BinarySearch binarySearch = new BinarySearch();
		int j = binarySearch.search(matrix[0], 1);
		if (j < 0) {
			j = matrix[0].length-1;
		}
		for (int i = 1; i < matrix.length; i++) {
			while (j >= 0 && matrix[i][j] == 1) {
				j--;
				maxRowIndex = i;
			}
		}
		return maxRowIndex;
	}
	
	
	public static void main(String[] args) {
		int[][] binaryMatrix = {
				{0,0,0,0,0},
				{0,0,0,1,1},
				{0,0,0,0,1},
				{0,0,1,1,1}
				
		};
		BinaryMatrixRowsWithMax1s obj = new BinaryMatrixRowsWithMax1s();
		System.out.println(obj.getMaxOnesInMatrix(binaryMatrix));
	}
}
