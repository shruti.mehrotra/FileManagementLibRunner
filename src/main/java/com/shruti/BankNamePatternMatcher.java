package com.shruti;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.opencsv.CSVReader;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class BankNamePatternMatcher {
	public static void main(String[] args) {
		String binFilePath = "/home/standby/Payments/RupayBins/Active Bin 12 Apr.xlsx";
		try {

			FileInputStream file = new FileInputStream(binFilePath);

			// Create Workbook instance holding reference to .xlsx file
			XSSFWorkbook workbook = new XSSFWorkbook(file);

			// Get first/desired sheet from the workbook
			XSSFSheet sheet = workbook.getSheetAt(0);

			// Iterate through each rows one by one
			Iterator<Row> rowIterator = sheet.iterator();
			Row headerRow = rowIterator.next();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// For each row, iterate through all the columns
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					// Check the cell type and format accordingly
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_NUMERIC:
						System.out.print(cell.getNumericCellValue());
						break;
					case Cell.CELL_TYPE_STRING:
						System.out.print(cell.getStringCellValue());
						System.out.println("Matching issuer mapping for input:" + getMatchingIssuerMappingForInput(cell.getStringCellValue()));
						break;
					}
				}
				System.out.println("");
			}
			file.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static String createInsertStatement(String[] row) {
		StringBuilder insertStmt = new StringBuilder();
		insertStmt.append(
				"INSERT INTO card_bin_range (bin_range_id, bin_tail, card_type, card_nature, status, bin_head, fk_card_issuer_id) VALUES (");
		int count = 0;
		for (Object val : row) {
			if (!(val instanceof Long) && !(val instanceof Double) && !(val instanceof Integer)
					&& !val.toString().contains("select")) {
				insertStmt.append("'").append(val).append("'");
			} else {
				insertStmt.append(val);
			}
			count++;
			if (count < row.length) {
				insertStmt.append(",");
			}
		}
		insertStmt.append(");");
		return insertStmt.toString();
	}

	public static IssuerCodeMapping getMatchingIssuerMappingForInput(String inputBankName) {
		IssuerCodeMapping issuerCodeMapping = new IssuerCodeMapping();
		String dbDumpFilePath = "/home/standby/Payments/issuer_card_mapping_dump.csv";
		try {

			CSVReader reader = readFile(dbDumpFilePath);
			String[] nextLine;

			nextLine = reader.readNext();
			while ((nextLine = reader.readNext()) != null) {
				String id = nextLine[0];
				String bankCode = nextLine[1];
				String bankName = nextLine[2];
				try {
					// 1. Match the exact string
					if (inputBankName.contains(bankName)) {
						issuerCodeMapping = getIssuerCodeMapping(id, bankName, bankCode);
						break;
					}
					// 2. Match with input bank short form
					String bankShortForm = "";
					String[] splittedName = StringUtils.split(inputBankName, " ");
					for (String name : splittedName) {
						String s1 = name.substring(0, 1).toUpperCase();
						bankShortForm += s1;
					}
					if (bankShortForm.contains(bankName)) {
						issuerCodeMapping = getIssuerCodeMapping(id, bankName, bankCode);
						break;
					}
					// 3. Match by each splitted name of the input bank string
					if (bankName.contains(splittedName[0]) && bankName.contains(splittedName[1])) {
						issuerCodeMapping = getIssuerCodeMapping(id, bankName, bankCode);
						break;
					}

					if (bankName.contains(splittedName[1]) && bankName.contains(splittedName[2])) {
						issuerCodeMapping = getIssuerCodeMapping(id, bankName, bankCode);
						break;
					}
				} catch (Exception e) {
					System.out.println("Error occured Error: " + e);
				}
			}

		} catch (Exception e) {
			System.out.println("Error occured while parsing Error: " + e);
		}
		return issuerCodeMapping;
	}

	public static IssuerCodeMapping getIssuerCodeMapping(String id, String bankName, String bankCode) {
		IssuerCodeMapping issuerCodeMapping = new IssuerCodeMapping();
		issuerCodeMapping.setBankCode(bankCode);
		issuerCodeMapping.setBankName(bankName);
		issuerCodeMapping.setId(id);
		return issuerCodeMapping;
	}

	public static CSVReader readFile(String filePath) throws FileNotFoundException {
		return new CSVReader(new FileReader(filePath));
	}

	@Data
	@NoArgsConstructor
	@ToString
	static class IssuerCodeMapping {
		String id;
		String bankCode;
		String bankName;
	}
}
