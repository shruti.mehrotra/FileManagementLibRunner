package com.shruti.batch.constants;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 17-Mar-2017
 */

public class StepConstant {
   public static final String DATA_STEP = "processDataStep";
   public static final String FILE_SNAPSHOT_STEP = "FileSnapshotStep";
   public static final String FILE_CREATION_STEP = "FileCreationStep";
   public static final String FILE_UPLOAD_STEP = "FileUploadStep";
   public static final String SEND_EMAIL_STEP = "SendEmailStep";
   public static final String POST_UPLOAD_UPDATE_STEP = "PostUploadUpdateStep";
   public static final String UNDERSCORE = "_";

   public static String getProcessDataStepName() {
      return DATA_STEP;
   }

   public static String getFileSnapshotStepName(String jobName) {
      return jobName + UNDERSCORE + FILE_SNAPSHOT_STEP;
   }

   public static String getFileCreationStepName(String jobName) {
      return jobName + UNDERSCORE + FILE_CREATION_STEP;
   }
   
   public static String getFileUploadStepName(String jobName) {
	  return jobName + UNDERSCORE + FILE_UPLOAD_STEP;
   }
   
   public static String getPostUploadUpdateStepName(String jobName) {
		  return jobName + UNDERSCORE + POST_UPLOAD_UPDATE_STEP;
	   }

   public static String getSendEmailStepName(String jobName) {
      return jobName + UNDERSCORE + SEND_EMAIL_STEP;
   }
}
