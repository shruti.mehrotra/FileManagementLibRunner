package com.shruti.batch.constants;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com) 
 * created on 17-Mar-2017
 */
public interface BatchConstants {
	public String FILE_NAME = "fileName";
	
	public String FILE_TYPE = "fileType";
	
	public String KEY_VAL_DB_OP_LIST = "keyValDbOpList";
	
	public String REQUEST_KEY_VAL_PAIR = "requestKeyValPair";
	
	public String JOB_FAIL_ATTEMPT_SEQ = "failAttemptSeq";

	public String JOB_NAME_FILE_STATUS_MANAGEMENT = "FileStatusMgmt";
}
