package com.shruti.batch.handler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.freecharge.batch.handlers.MapBasedCleanableJobHandler;
import com.freecharge.dao.dto.KeyValDto;
import com.freecharge.dao.dto.KeyValDtoList;
import com.freecharge.dao.dto.KeyValWithDbOperatorDto;
import com.freecharge.dao.dto.KeyValWithDbOperatorList;
import com.freecharge.dao.dto.operators.DBOperator;
import com.freecharge.dao.dto.operators.PairSeparator;
import com.freecharge.util.AppUtil;
import com.freecharge.util.DateUtil;
import com.google.gson.Gson;
import com.shruti.batch.constants.BatchConstants;
import com.shruti.batch.constants.StepConstant;
import com.shruti.batch.steps.FileCreationStep;
import com.shruti.batch.steps.FileSnapshotStep;
import com.shruti.batch.steps.FileUploadStep;
import com.shruti.batch.steps.PostUploadUpdateStep;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Slf4j
@Component
public class FileStatusManagementHandler extends MapBasedCleanableJobHandler {
	
	@Autowired
	private JobBuilderFactory jobs;

	@Autowired
	private StepBuilderFactory steps;

	@Autowired
	private FileSnapshotStep fileSnapshotTasklet;

	@Autowired
	private FileCreationStep fileCreationTasklet;

	@Autowired
	private FileUploadStep fileUploadTasklet;

	@Autowired
	private PostUploadUpdateStep postUploadUpdateTasklet;

	/*
	 * @Autowired private SendEmailStep sendEmailTasklet;
	 */

	Map<String, String> jobData = null;

	@Override
	public Map<String, String> getJobData() {
		return jobData;
	}

	public String processFile(String fileType, Date startTime, Date endTime, String merchantId) {

		String jobName = BatchConstants.JOB_NAME_FILE_STATUS_MANAGEMENT;
		Step fileSnapshotStep = steps.get(StepConstant.getFileSnapshotStepName(jobName)).tasklet(fileSnapshotTasklet)
				.build();
		Step fileCreationStep = steps.get(StepConstant.getFileCreationStepName(jobName)).tasklet(fileCreationTasklet)
				.build();

		Step fileUploaderStep = steps.get(StepConstant.getFileUploadStepName(jobName)).tasklet(fileUploadTasklet)
				.build();

		Step postFileUploadUpdateStep = steps.get(StepConstant.getPostUploadUpdateStepName(jobName))
				.tasklet(postUploadUpdateTasklet).build();
		/*
		 * Step sendEmailStep = steps
		 * .get(StepConstant.getSendEmailStepName(jobName))
		 * .tasklet(sendEmailTasklet).build();
		 */

		Job job = jobs.get(jobName)/* .preventRestart() */
				.start(fileSnapshotStep)
				.next(fileCreationStep)
				.next(fileUploaderStep)
				.next(postFileUploadUpdateStep)
				// .next(sendEmailStep)
				.build();

		JobParametersBuilder builder = new JobParametersBuilder();
		List<KeyValWithDbOperatorDto> keyValPairWithDbOperatorList = new ArrayList<>();
		
		keyValPairWithDbOperatorList.add(new KeyValWithDbOperatorDto("refunded_on", AppUtil.quoteString(DateUtil.formatDate(startTime, DateUtil.YYYYDDMM_HYPHENED_HHMMSS_COLONED)),
				DBOperator.GREATER_THAN_EQUAL_TO, PairSeparator.AND));
		keyValPairWithDbOperatorList.add(new KeyValWithDbOperatorDto("refunded_on", AppUtil.quoteString(DateUtil.formatDate(endTime, DateUtil.YYYYDDMM_HYPHENED_HHMMSS_COLONED)),
				DBOperator.LESS_THAN_EQUAL_TO, null));
		/*keyValPairWithDbOperatorList.add(new KeyValWithDbOperatorDto("refunded_on",
				AppUtil.quoteString("2017-01-20 00:00:00"), DBOperator.GREATER_THAN_EQUAL_TO, PairSeparator.AND));
		keyValPairWithDbOperatorList.add(new KeyValWithDbOperatorDto("refunded_on",
				AppUtil.quoteString("2017-04-30 00:00:00"), DBOperator.LESS_THAN_EQUAL_TO, null));*/
		KeyValWithDbOperatorList keyValWithDbOperatorList = new KeyValWithDbOperatorList(keyValPairWithDbOperatorList);

		List<KeyValDto> keyValDtoList = new ArrayList<>();
		keyValDtoList.add(new KeyValDto("merchantId", merchantId));
		KeyValDtoList requestPairList = new KeyValDtoList();
		requestPairList.setKeyValDtoList(keyValDtoList);
		buildJobParameters(builder, fileType, keyValWithDbOperatorList, requestPairList);
		log.info("Launching job for file status management...");
		launchJob(job, builder);

		return null;
	}

	private void buildJobParameters(JobParametersBuilder builder, String fileType, KeyValWithDbOperatorList keyValList,
			KeyValDtoList keyValDtoList) {

		builder.addString(BatchConstants.FILE_TYPE, fileType);
		builder.addString(BatchConstants.KEY_VAL_DB_OP_LIST, new Gson().toJson(keyValList));
		builder.addString(BatchConstants.REQUEST_KEY_VAL_PAIR, new Gson().toJson(keyValDtoList));
	}

}
