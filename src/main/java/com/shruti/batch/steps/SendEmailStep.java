package com.shruti.batch.steps;

import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

import com.freecharge.batch.TaskWithRetry;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com) 
 * created on 17-Mar-2017
 */
@Component
public class SendEmailStep extends TaskWithRetry 
							implements Tasklet {

	@Override
	public RepeatStatus execute(StepContribution paramStepContribution,
			ChunkContext paramChunkContext) throws Exception {

		JobParameters jobParameters = paramChunkContext.getStepContext()
				.getStepExecution().getJobParameters();
		final String localFilePath = jobParameters
				.getString("");
		try {
			
			processWithRetry(localFilePath);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	protected void task(Object... args) throws Exception {
		String localFilePath = args[0].toString();
		System.out.println(localFilePath);
	}


}
