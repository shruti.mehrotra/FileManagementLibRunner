package com.shruti.batch.steps;

import java.util.Date;

import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.freecharge.batch.TaskWithRetry;
import com.freecharge.exceptions.FileManagementLibException;
import com.freecharge.file.generator.IFileGeneratorService;
import com.freecharge.model.dto.TimeRange;
import com.freecharge.model.request.FileGenRequest;
import com.freecharge.model.response.FileGenListResponse;
import com.freecharge.model.type.FileConfigType;
import com.freecharge.util.DateUtil;
import com.shruti.batch.FileManagementBatchConfig;
import com.shruti.batch.constants.BatchConstants;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com) 
 * created on 17-Mar-2017
 */
@Component
@Slf4j
public class FileCreationStep extends TaskWithRetry 
							implements Tasklet {

	@Autowired
	private FileManagementBatchConfig batchConfig;
	
	@Autowired
	@Qualifier("FileGeneratorServiceImpl")
	private IFileGeneratorService fileGenService;
	
	@Override
	public RepeatStatus execute(StepContribution paramStepContribution,
			ChunkContext paramChunkContext) throws Exception {

		JobParameters jobParameters = paramChunkContext.getStepContext()
				.getStepExecution().getJobParameters();
		final String fileType = jobParameters
				.getString(BatchConstants.FILE_TYPE);
		final String keyValDBOpList = jobParameters
				.getString(BatchConstants.KEY_VAL_DB_OP_LIST);
		final String requestKeyValPair = jobParameters
				.getString(BatchConstants.REQUEST_KEY_VAL_PAIR);
		Date currentDate = DateUtil.currentDay();
		final TimeRange timeRange = new TimeRange(DateUtil.subtractDay(currentDate, 1), currentDate);
		try {
			processWithRetry(fileType, keyValDBOpList, requestKeyValPair, timeRange);
		} catch (FileManagementLibException e) {
			throw e;
		} catch (Throwable e) {
			throw new RuntimeException("Error while generating file: ", e);
		}
		return RepeatStatus.FINISHED;
	}
	
	@Override
	protected void task(Object... args) throws Exception {
		batchConfig.setParamsFromInput(args);
		String fileType = batchConfig.getFileType();
		TimeRange timeRange = (TimeRange)args[3];
		
		FileGenRequest request = new FileGenRequest();
		request.setFileConfigType(FileConfigType.valueOf(fileType));
		request.setUpdateTSRange(timeRange);
		FileGenListResponse response = fileGenService.generateFileByTimeRange(request);
		log.info("FileGen response for file gen step with request params: request: {} and response: {}", request.toString(), response.toString());
	}


}
