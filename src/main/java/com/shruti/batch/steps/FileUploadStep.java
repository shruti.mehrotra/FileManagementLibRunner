package com.shruti.batch.steps;

import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.freecharge.batch.TaskWithRetry;
import com.freecharge.exceptions.FileManagementLibException;
import com.freecharge.file.uploader.IFileUploaderService;
import com.freecharge.model.response.FileUploadResponse;
import com.freecharge.model.type.FileConfigType;
import com.shruti.batch.FileManagementBatchConfig;
import com.shruti.batch.constants.BatchConstants;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com) created on
 *         17-Mar-2017
 */
@Component
@Slf4j
public class FileUploadStep extends TaskWithRetry implements Tasklet {

	@Autowired
	@Qualifier("FileUploaderServiceImpl")
	private IFileUploaderService uploaderService;

	@Autowired
	private FileManagementBatchConfig batchConfig;

	@Autowired
	@Getter
	FileUploadResponse uploadResponse;

	@Override
	public RepeatStatus execute(StepContribution paramStepContribution, ChunkContext paramChunkContext)
			throws Exception {

		JobParameters jobParameters = paramChunkContext.getStepContext().getStepExecution().getJobParameters();
		final String fileType = jobParameters.getString(BatchConstants.FILE_TYPE);
		final String keyValDBOpList = jobParameters.getString(BatchConstants.KEY_VAL_DB_OP_LIST);
		final String requestKeyValPair = jobParameters.getString(BatchConstants.REQUEST_KEY_VAL_PAIR);
		try {

			processWithRetry(fileType, keyValDBOpList, requestKeyValPair);
		} catch (FileManagementLibException e) {
			throw e;
		} catch (Throwable e) {
			throw new RuntimeException("Error while uploading file: ", e);
		}
		return RepeatStatus.FINISHED;
	}

	@Override
	protected void task(Object... args) throws Exception {
		batchConfig.setParamsFromInput(args);
		String fileType = batchConfig.getFileType();
		uploadResponse = uploaderService.uploadFile(FileConfigType.valueOf(fileType));
		log.info("Upload response for file upload step with request params: fileType: {}", fileType,
				uploadResponse != null ? uploadResponse.toString() : null);

	}

}