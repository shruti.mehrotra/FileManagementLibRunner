package com.shruti.batch;

import org.springframework.stereotype.Component;

import com.freecharge.dao.dto.KeyValDtoList;
import com.freecharge.dao.dto.KeyValWithDbOperatorList;
import com.google.gson.Gson;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Component
public class FileManagementBatchConfig {

	@Getter
	@Setter
	private String fileType;

	@Getter
	@Setter
	private KeyValWithDbOperatorList keyValWithDbOpList;

	@Getter
	@Setter
	private KeyValDtoList requestKeyValPairs;

	public void setParamsFromInput(Object... args) {
		if (args[0] != null) {
			fileType = args[0].toString();
		}
		if (args[1] != null) {
			keyValWithDbOpList = new Gson().fromJson(args[1].toString(), KeyValWithDbOperatorList.class);
		}
		if (args[2] != null) {
			requestKeyValPairs = new Gson().fromJson(args[2].toString(), KeyValDtoList.class);
		}

	}
}
